# cloud native summit - links

the following is a collection of links associated with the talks moderated for the April 2020 [Cloud Native (Online) Summit](https://cloudnativesummit.online).

# "SIG-Culture" and Well-being WG
A chat moderated by [Ariel Jatib](https://twitter.com/arieljatib) Mark Coleman [@mrmrcoleman](https://twitter.com/mrmrcoleman) and Stephen Augustus [@stephenaugustus](https://twitter.com/stephenaugustus) about the culture of the cloud native community and the well-being working group originally created to improve the comfort, health, and happiness of KubeCon + CloudNativeCon attendees. 

- [slide deck](https://docs.google.com/presentation/d/1yvNSpBOleWo__h7ijbmnXDLgbq-tYThC48ptrUpb9EA/edit?usp=sharing)
- [CNCF Wellbeing WG Mailing List](https://lists.cncf.io/g/cncf-wellbeing-wg)
- PDF [OSRI Conference Handbook - Guidelines for Mental Health at Conferences](https://events19.linuxfoundation.org/wp-content/uploads/2019/11/kubecon-na2019-OSMI.pdf)
- [Keep Cloud Native Well - Kubecon San Diego 2019](https://events19.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2019/attend/keep-cloud-native-well/)
- CNCF Blog Post, a guide to well-being during COVID-19 [We're all in this together](https://www.cncf.io/blog/2020/04/03/were-all-in-this-together-a-wellness-guide-from-the-cncf-well-being-working-group/)

# SIGs : Contributing - Why and How
A chat w Taylor Dolezal, Puja Abbassi, Stephen Augustus about the why (value) and how of contributing to Kubernetes SIGs (Special interest Groups).

- [slide deck   ](https://docs.google.com/presentation/d/15WP1C0tp5XJAPaZKMu06BusS3mWVYUyTvGPWReBBaDA/edit?usp=sharing)
- [Kuberntes SIGs and Working Groups List](https://github.com/kubernetes/community/blob/master/sig-list.md)
- [CNCF Mailing Lists](https://lists.cncf.io/g/main)
- [Kuberentes Slack](https://slack.k8s.io/)
- [CNCF Slack](https://slack.cncf.io/)

